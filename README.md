# Alert Flash Messages

This is a Laravel ready alert messages package.

### Installation
First, download the package.
If using Laravel 5, include the service provider within config/app.php.
```php
'providers' => [
    NerdMonkeySoft\Alert\AlertServiceProvider::class
];
```
Then add a facade alias to this same file at the bottom:
```php
'aliases' => [
    'Alert'     => NerdMonkeySoft\Alert\Alert::class
];
```

### Dependencies

For the dependencies please check package.json. You will need:

* Bootstrap,
* Simple Line Icons.

Or, if you want to use other styling, simply publish and modify the view (go to "modify the view").

### Usage
Within your controllers or in any other place you can simply:
```php
Alert::error('Some error message');
```

You may also use:
* `Alert::info('Some info message); `
* `Alert::success('Some info message); `
* `Alert::error('Some info message); `

Then, just append to your layout view:
```php
@include('alert::message')
```
### Options
Also there are two additional options:
* [important] - allows to add the alert-important class in message div,
* [multiple] - allows to add multiple messages.

We can use method chaining to add these option, so you can just:
```php
Alert::info('This is important')->important();
```
If you need to pass multiple messages:
```php
Alert::info('This is important')->success('And this is a success message')->multiple();
```
..or why not to try everything:
```php
Alert::error('Error message')->success('Success message')->info('Info message')->multiple()->important();
```

### Example

Within AuthController you may want to flash a success logout message.

```php
public function logout()
{
    Alert::success('You have been successfully logged out.')->important();
    
    return redirect()->route('login');
}
```
Then, within your blade template simply:
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Flash Alerts</title>
</head>
<body>
    <div class="container">
        @include('alert::message')
    </div>
</body>
</html>
```

### Need to modify the view?
If you need to modify the message view, you can just:
```
php artisan vendor:publish
```

That's it. Have fun.