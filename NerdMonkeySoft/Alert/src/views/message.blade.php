
@if(Session::has('NerdMonkeySoft\Alert\Options\Multiple'))

    @if(Session::has('NerdMonkeySoft\Alert\Success'))
        @include('alert::partials.success', ['message' => Session::get('NerdMonkeySoft\Alert\Success')])
    @endif

    @if(Session::has('NerdMonkeySoft\Alert\Error'))
        @include('alert::partials.error', ['message' => Session::get('NerdMonkeySoft\Alert\Error')])
    @endif

    @if(Session::has('NerdMonkeySoft\Alert\Info'))
        @include('alert::partials.info', ['message' => Session::get('NerdMonkeySoft\Alert\Info')])
    @endif

@else

    @if(Session::has('NerdMonkeySoft\Alert\Success'))
        @include('alert::partials.success', ['message' => Session::get('NerdMonkeySoft\Alert\Success')])

    @elseif(Session::has('NerdMonkeySoft\Alert\Error'))
        @include('alert::partials.error', ['message' => Session::get('NerdMonkeySoft\Alert\Error')])

    @elseif(Session::has('NerdMonkeySoft\Alert\Info'))
        @include('alert::partials.info', ['message' => Session::get('NerdMonkeySoft\Alert\Info')])
    @endif

@endif





