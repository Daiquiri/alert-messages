<div class="@if(Session::has('NerdMonkeySoft\Alert\Options\Important'))alert-important @else alert-message @endif msg-error">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
    <span class="sli-close">{!! $message !!}</span>
</div>