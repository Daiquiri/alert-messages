<?php namespace NerdMonkeySoft\Alert;


class FlashMessages
{
    /**
     * Flashes the messages to the session.
     *
     * @var AlertSessionStore
     */
    private $session;

    /**
     * Create a new flash message instance.
     *
     * @param AlertSessionStore $session
     */
    public function __construct(AlertSessionStore $session)
    {
        $this->session = $session;
    }

    /**
     * Add the multiple message option.
     *
     * @return $this
     */
    public function multiple()
    {
        $this->session->flashOption('multiple', true);

        return $this;
    }

    /**
     * Add the important option.
     *
     * @return $this
     */
    public function important()
    {
        $this->session->flashOption('important', true);

        return $this;
    }

    /**
     * Flash an info (predefined) message.
     *
     * @param string $message
     * @return $this
     */
    public function info($message)
    {
        $this->storeMessage('info', $message);

        return $this;
    }

    /**
     * Flash an error (predefined) message.
     *
     * @param string $message
     * @return $this
     */
    public function error($message)
    {
        $this->storeMessage('error', $message);

        return $this;
    }

    /**
     * Flash a success (predefined) message.
     *
     * @param string $message
     * @return $this
     */
    public function success($message)
    {
        $this->storeMessage('success', $message);

        return $this;
    }

    /**
     * Put the messages into session storage.
     *
     * @param string $name
     * @param string $message
     * @return $this
     */
    private function storeMessage($name, $message)
    {
        $this->session->flash($name, $message);

        return $this;
    }
}