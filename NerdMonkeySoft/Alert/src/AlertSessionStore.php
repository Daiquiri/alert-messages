<?php namespace NerdMonkeySoft\Alert;

use Illuminate\Session\Store;

class AlertSessionStore
{
    /**
     * @var Store
     */
    private $session;

    /**
     * Session variable prefix.
     *
     * @var string
     */
    private $prefix = 'NerdMonkeySoft\Alert\\';


    /**
     * AlertSessionStore constructor.
     *
     * @param Store $session
     */
    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Flash a message to the session.
     *
     * @param string $name
     * @param string|array $data
     */
    public function flash($name, $data)
    {
        $this->session->flash($this->createFlashName($name), $data);
    }

    /**
     * Create a flash message name.
     *
     * Combine the prefix and name given. Capitalize first character of name attribute.
     *
     * @param string $name
     * @return string
     */
    private function createFlashName($name)
    {
        return $this->prefix . ucfirst($name);
    }

    /**
     * Flash an option to the session.
     *
     * @param string $name
     * @param $value
     */
    public function flashOption($name, $value)
    {
        $this->session->flash($this->createFlashOptionName($name), $value);
    }

    /**
     * Create a flash option name.
     *
     * @param string $name
     * @return string
     */
    private function createFlashOptionName($name)
    {
        return $this->prefix . 'Options\\' . ucfirst($name);
    }
}